#!/bin/bash
set -e

# Navigate to project repo
pushd /yeodisplay

# if mount is enabled then build yeodisplay
if $YEODISPLAY_DEVELOPMENT_MOUNT_ENABLED; then
    ./install.sh
fi

# setup ros2 environment
source "/opt/ros/$ROS_DISTRO/setup.bash"

# source yeodisplay setup file
. ./install/setup.bash

popd

ros2 launch launch/yeodisplay.launch.py