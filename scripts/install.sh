#!/bin/bash
set -e

### Navigate to project repo
pushd /yeodisplay/

# setup ros2 environment
source "/opt/ros/$ROS_DISTRO/setup.bash"

# Build yeodisplay
rosdep install -i --from-path . --rosdistro $ROS_DISTRO -y
colcon build --packages-select yeodisplay
popd