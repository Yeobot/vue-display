#!/bin/bash
deploymentName="yeodisplay"
# overridePath="/home/bhacken/helm/deploy.yaml"
# overrides=""
# if [ -f ${overridePath} ]; then
#     overrides="-f ${overridePath}"
#     echo "overridePath: ${overridePath} overrides: ${overrides}"
# else
#     echo "No override file found in location: ${overridePath}"
# fi

echo "Deploying ${deploymentName}"
# Print yeodisplay deployment.yaml file with override variable
# helm template --name-template ${deploymentName} . -x ${overridePath} --values $overrides

helm install ${deploymentName} ./helm/ --namespace yeobot # $overrides