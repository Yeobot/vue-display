# yeodisplay

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Start database server
### Starting pouchdb server on port 5000 and choose file storage location
    pouchdb-server -p 5000 --help -d ~/yeobot-dev/yeodisplay/database

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### **Project setup instructions**
#### Install Node.js and verify the version installed
    sudo apt update
    sudo apt install nodejs
    node -v
#### Check available versions of node, upgrading Node.js, and verify the version installed (11/30/22 current version: 18.12.1)
    nvm ls-remote
    npm install v18.12.1
    nvm use v18.12.1
    node -v
#### Install the latest version of npm (11/30/22 current version: 9.1.2)
    npm install -g npm
    npm -v
#### Install Vue Client and verify the version installed (11/30/22 current version: 5.0.8)
    npm install -g @vue/cli
    vue -V
#### Uninstall Vue Client
    npm uninstall -g @vue/cli
#### Create a Vue package in a local directory
    vue create .
#### Fix a npm dependency tree error "--legacy-peer-deps restores peerDependency installation behavior from NPM v4 thru v6"
    npm config set legacy-peer-deps true
#### Example of adding a dependency
    pushd ~/yeobot-dev/yeodisplay
    npm i bootstrap-vue@2.23.1















     1983  npm install vue.draggable
 1984  npm i -S vuedraggable
 1985  npm install pouchdb
 1986  npm install -g pouchdb-server 
 1987  pouchdb-server -p 5000
 1988  cd yeobot-dev/
 1989  ls
 1990  cd yeodisplay/
 1991  code . &
 1992  npm run serve
 1993  npm run serve --fix
 1994  npm run serve
 1995  npm i --save-dev @types/pouchdb
 1996  npm run serve
 1997  npm install -D @types/pouchdb
 1998  npm run build
 1999  npm run serve
 2000  code . &
 2001  ls
 2002  history
