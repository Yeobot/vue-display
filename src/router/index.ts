import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import FarmDesignerView from '../views/FarmDesignerView.vue'
Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/FarmDesigner',
    name: 'Farm Designer',
    component: FarmDesignerView
  },
  {
    path: '/Controller',
    name: 'Controller',
    component: () => import('../views/ControllerView.vue')
  },
  {
    path: '/SensorData',
    name: 'Sensor Data',
    component: () => import('../views/SensorDataView.vue')
  },
  {
    path: '/Devices',
    name: 'Devices',
    component: () => import('../views/DevicesView.vue')
  },
  {
    path: '/Task',
    name: 'Task',
    component: () => import('../views/TaskView.vue')
  },
  {
    path: '/Schedule',
    name: 'Schedule',
    component: () => import('../views/ScheduleView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
