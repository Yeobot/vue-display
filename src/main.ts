import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Simple from './components/simple.vue'
import DndTest from './components/Dnd-test.vue'
import NavBars from './components/NavBars.vue'
import HelloWorld from './components/HelloWorld.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import PortalVue from 'portal-vue'
import VueDraggable from 'vue-draggable'
// import VueGridLayout from '../node_modules/vue-grid-layout'

// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
// Make PortalVue available throughout your project
Vue.use(PortalVue)

Vue.use(VueDraggable)
// Vue.use(VueGridLayout)

Vue.config.productionTip = false
Vue.component('dnd-name', DndTest)
Vue.component('simple-name', Simple)
Vue.component('nav-bars', NavBars)
Vue.component('hello-world', HelloWorld)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
